// JavaScript Document
$ = jQuery;
screen_w = jQuery(window).width();
screen_h = jQuery(window).height();

jQuery(document).ready(function(){

		jQuery('.h_banner, .h_banner_bg_p, .cl_h_sec5_popup').css({'height':screen_h,'width':'100%'}); 
		jQuery('.full_screen').css({'height':jQuery(window).height()-0,'width':jQuery(window).width()});
		
		jQuery(window).resize(function() {
			screen_w = jQuery(window).width();
			screen_h = jQuery(window).height();  
		
		  jQuery('.h_banner, .h_banner_bg_p, .cl_h_sec5_popup').css({'height':screen_h,'width':'100%'});
		  jQuery('.full_screen').css({'height':jQuery(window).height()-0,'width':jQuery(window).width()});
		  
		  if(jQuery(window).width() = 768)
		  {
			jQuery('#main-nav').addClass('main_nav_desktop');
		  }
		  else
		  {
			  jQuery('#main-nav').removeClass('main_nav_desktop');
		  }
		  
		});
		
		
<!--/* @@@@======================================@@@   Main Contact PopUp   @@@====================================@@@  */-->		

/* Mail function */

function ClPopUpMail() {
	var name = jQuery("#jgCName").val();
	var email = jQuery("#jgCEmail").val();
	var phone = jQuery("#jgCPhone").val(); 
	var msg = jQuery("#jgCMsg").val();
    var pageurl = window.location.href;
	
	var Source = jQuery(".Source").val();
	var Medium = jQuery(".Medium").val();
	var Campaign = jQuery(".Campaign").val();
	var KeyWord = jQuery(".KeyWord").val();
	var Cookie_Country = jQuery(".Cookie_Country").val();
    var pagetitle = $(document).find("title").text();
	
	jQuery.ajax({
		type: 'post',
		url: '/wp-content/themes/wpclicklabs/cta/cta.php',
		data: {
			name: name,
			email: email,
			phone: phone,
			msg:msg,
			pageurl: pageurl,
			Source:Source,
			Medium:Medium,
			Campaign:Campaign,
			KeyWord:KeyWord,
			Cookie_Country:Cookie_Country,
            pagetitle:pagetitle,
			formid:1,
			type:1
			},
		dataType: 'JSON',
		success: function(data) {
			console.log(data);
			console.log(data['message']);
		},
	});
	return true;
}

function Contact_submit() {
    
    jQuery.ajax({
        type: 'post',
        url: '/wp-content/themes/wpclicklabs/cta/contact_submitted.php',
        data: {},
        dataType: 'JSON',
        success: function(data) {
        },
    });
    return true;
}

	
    jQuery('input#clMSubmit').click(function() {
			var Error = false;
			//var uberfor = jQuery("#uberfor").val();
	        var name = jQuery("#jgCName").val();
	        var email = jQuery("#jgCEmail").val();
	        var tel = jQuery("#jgCPhone").val(); 
			var atpos=email.indexOf("@");
			var dotpos=email.lastIndexOf(".");
			var numeric = /^[0-9+-]+$/;
			var txwLgErrorMsg = "Please fill the required fields";
         	//alert("Hi");
			
			/*if(uberfor.length == 0){
			var Error = true;
			jQuery('#uberfor').css('border-bottom','1px solid red');
			return false;
			 } 
			else  
			 {
			  jQuery('#uberfor').css('border-bottom','1px solid #cdcdcd');
			 }*/
			 
			if(name.length == 0){
			var Error = true;
			jQuery('#jgCName').css('border','1px solid red');
			return false;
			 } 
			else  
			 {
			  jQuery('#jgCName').css('border','1px solid #cdcdcd');
			 }
			 
			if(email==0)
			{
			  jQuery('#jgCEmail').css('border','1px solid red');
			  //jQuery("#txwLgErrorMsg").html("Please fill Email ID");
			return false;
			}
			
				if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
				  {
				  var Error = true;
				   jQuery('#jgCEmail').css('border','1px solid red');
				  return false;
				  }
				  if(email!=0)
				  {
				jQuery('#jgCEmail').css('border','1px solid #cdcdcd');
				  }
				  	
			Contact_submit();	  
		    ClPopUpMail();
		    window.top.location = 'http://click-labs.com/thank-you/';
			if(Error == false){
				  jQuery("#clMSubmit").click(function(){
				  //this.value='';
				  this.disabled='disabled';
				  });
			   } 	
		  });

	
	jQuery("#jgCEmail").keyup(function () {
    if (this.value.length > 0) {
    //$(this).prev('.digits').focus();
    //$(this).prev('.digits').trigger( "select" );
        console.log(jQuery("#jgCEmail").val());
    }  });

    jQuery( "#jgCEmail" ).keyup(function() {
        email_keyup();
    });
    jQuery(document).on('blur', '#jgCEmail', function() {
        email_blur();
    });
    jQuery( "#jgCName" ).keyup(function() {
        name_keyup();
    });
    jQuery(document).on('blur', '#jgCName', function() {
        name_blur();
    });
    jQuery( "#jgCPhone" ).keyup(function() {
        phone_keyup();
    });
    jQuery(document).on('blur', '#jgCPhone', function() {
        phone_blur();
    });
		
});

function email_keyup()
{  
        var email = jQuery("#jgCEmail").val();
        jQuery.ajax({
                    type: 'post',
                    url: '/wp-content/themes/wpclicklabs/cta/database.php',
                    data: {
                        email:email,
                        form: 1,
                        type:'email_up'
                        },
                    dataType: 'Json',
                    success: function(data) {
                        console.log(data);
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                        //console.log("Status: " + textStatus); 
                        } 
                });
}
function email_blur()
{  
        var email = jQuery("#jgCEmail").val();
        jQuery.ajax({
                    type: 'post',
                    url: '/wp-content/themes/wpclicklabs/cta/database.php',
                    data: {
                        email:email,
                        form: 1,
                        type:'email_out'
                        },
                    dataType: 'Json',
                    success: function(data) {
                        console.log(data);
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                        //console.log("Status: " + textStatus); 
                        } 
                });
}

function name_keyup()
{  
        var name = jQuery("#jgCName").val();
        jQuery.ajax({
                    type: 'post',
                    url: '/wp-content/themes/wpclicklabs/cta/database.php',
                    data: {
                        name:name,
                        form: 1,
                        type:'name_up'
                        },
                    dataType: 'Json',
                    success: function(data) {
                        console.log(data);
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                        //console.log("Status: " + textStatus); 
                        } 
                });
}
function name_blur()
{  
        var name = jQuery("#jgCName").val();
        jQuery.ajax({
                    type: 'post',
                    url: '/wp-content/themes/wpclicklabs/cta/database.php',
                    data: {
                        name:name,
                        form: 1,
                        type:'name_out'
                        },
                    dataType: 'Json',
                    success: function(data) {
                        console.log(data);
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                        //console.log("Status: " + textStatus); 
                        } 
                });
}

function phone_keyup()
{  
        var phone = jQuery("#jgCPhone").val();
        jQuery.ajax({
                    type: 'post',
                    url: '/wp-content/themes/wpclicklabs/cta/database.php',
                    data: {
                        phone:phone,
                        form: 1,
                        type:'phone_up'
                        },
                    dataType: 'Json',
                    success: function(data) {
                        console.log(data);
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                        //console.log("Status: " + textStatus); 
                        } 
                });
}
function phone_blur()
{  
        var phone = jQuery("#jgCPhone").val();
        jQuery.ajax({
                    type: 'post',
                    url: '/wp-content/themes/wpclicklabs/cta/database.php',
                    data: {
                        phone:phone,
                        form: 1,
                        type:'phone_out'
                        },
                    dataType: 'Json',
                    success: function(data) {
                        console.log(data);
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                        //console.log("Status: " + textStatus); 
                        } 
                });
}


jQuery(document).ready(function(){
      jQuery('.jg_click_popup').click(function() { 
	  
	  if(jQuery(window).width() <= 600 ) { jg_click_popup_position = 'absolute' } else { jg_click_popup_position = 'fixed' }
        jQuery('#jg_contact_popup').bPopup({    
          //easing: 'easeOutBack', //uses jQuery easing plugin
          speed: 400,
          transition: 'slideDown',
          zIndex: 2,
		  positionStyle: jg_click_popup_position,
		  modal:false,
          modalClose: true  //Should the popup close on click on overlay
            });

         });
      jQuery('#jg_contact_popup_close').click(function() {
        var bPopup = jQuery('#jg_contact_popup').bPopup({
            easing: 'easeOutBack', //uses jQuery easing plugin
            speed: 650,
            transition: 'slideUp'
            });
        bPopup.close(); 
          });
		  

$("#jg_contact_popup_close").hover(function () { 
	$(this).toggleClass("rotate180");
	 });


<!--/* @@@@======================================@@@ ///  Main Contact PopUp   @@@====================================@@@  */-->

<!--/* @@@@======================================@@@  Blog/Single post JS   @@@====================================@@@  */-->

/*	Newsletter form blog Top*/

function newsletter_sub() {
				  var email1 = jQuery("#email").val();
				  var name = jQuery("#name").val();
				  var list = jQuery("#list").val();
                  var url = window.location.href;
               jQuery.ajax({
                    type: 'POST',
                    url: '/wp-content/themes/wpclicklabs/cta/cta.php',
                    data: {email1:email1, name:name, list:list, url:url, type:2},
                    dataType: 'JSON',
				    //processData: false,
                    success: function(data) {
                        console.log(data);

                        //alert(data['log']);

                        if (data['log'] == 1) {
							console.log(data);
                        }
                        else if (data['log'] == 0) {

                            document.getElementById("email").style.backgroundColor = "#F8CCCC";
                        }
                        else {

                        }
                       
                    },
					
                });

                return true;
            }
			
/*	Newsletter form blog sidebar*/
			
		function newsletter_b_sidebar_sub() {
				  var email1 = jQuery("#email1").val();
				  var name = jQuery("#name1").val();
				  var list = jQuery("#list1").val();
                  var url = window.location.href;
               jQuery.ajax({
                    type: 'POST',
					url: '/wp-content/themes/wpclicklabs/cta/cta.php',
                    data: {email1:email1, name:name, list:list, url:url, type:2},
                    dataType: 'JSON',
				    //processData: false,
                    success: function(data) {
                        console.log(data);
                        if (data['log'] == 1) {
							console.log(data);
                        }
                        else if (data['log'] == 0) {

                            //document.getElementById("email").style.backgroundColor = "#F8CCCC";
                        }
                        else {
                        }                      
                    },					
                });
                return true;
            }
/*	/// Newsletter form blog sidebar*/

jQuery(document).ready(function() {
	
/*Blog Top Newsletter form Validation*/
	 jQuery('input.news_submit').click(function() {
		 
	    var Error = false;
		var email = document.news_letter.email.value;
		var atpos=email.indexOf("@");
	    var dotpos=email.lastIndexOf(".");
	  
		if(email==0)
		{
		document.getElementById("email").style.border="1px solid red";
		return false;
		}
		
			  if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
				{
				var Error = true;
				document.getElementById("email").style.border="1px solid red";
				return false;
				}
				if(email!=0)
				{
				  document.getElementById("email").style.border="1px solid #CCC";
				}	
					
		 newsletter_sub();
  //ga('send', 'event', 'topnavsubscribebutton', 'click', 'nav-buttons');
  	jQuery('#email').val('')
  	jQuery("#success_msg").fadeIn(1000);
	 
		 if(Error == false){	
           
		    // To reset the email field value
		  // document.getElementById("success_msg").innerHTML="Subscribed";
		  //jQuery("#success_msg").fadeIn(1000);
		  jQuery("#success_msg").delay(5000).fadeOut(); // To hide the success message after some time		  
		  jQuery(".news_submit").click(function(){
		  //this.value='Submitting ..';
		  this.disabled='disabled';
		  });
		} 
		 
	});
	
	
/*Blog Sidebar Newsletter form Validation*/
	
	 jQuery('input.news_submit1').click(function() { 
	    var Error = false;
		var email = jQuery("#email1").val();
		var atpos=email.indexOf("@");
	    var dotpos=email.lastIndexOf(".");
	  
		if(email==0)
		{
		document.getElementById("email1").style.border="1px solid red";
		return false;
		}
		
			  if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
				{
				var Error = true;
				document.getElementById("email1").style.border="1px solid red";
				return false;
				}
				if(email!=0)
				{
				  document.getElementById("email1").style.border="1px solid #CCC";
				}	
			
			 newsletter_b_sidebar_sub();
			 jQuery("#success_msg1").fadeIn(1000);
		  jQuery("#news_letter1").fadeOut();			
             //ga('send', 'event', 'topnavsubscribebutton', 'click', 'nav-buttons');
		 
		 if(Error == false){	
          
		   jQuery('#email1').val('') // To reset the email field value
		  // document.getElementById("success_msg").innerHTML="Subscribed";
		  //jQuery("#success_msg1").fadeIn(1000);
		  //jQuery("#news_letter1").fadeOut(); // To hide the success message after some time
		  
		  /*jQuery(".news_submit").click(function(){
		  this.disabled='disabled';
		  });*/
		} 
		 
	});

});

<!--/* @@@@======================================@@@  /// Blog/Single post JS   @@@====================================@@@  */-->






  
 // Top nav button
  $('#menu').menupuncher({
	  color : '#000',
	  textcolor  : '#fff',
	  opacity: '0.8',
  }); 
  
 
//jQuery('.cl_fixed_nav').css('background','#FFFFFF');
jQuery('.cl_fixed_nav').addClass('cl_H_fixed_nav bg_white box_shw'); 
jQuery(".cl_fixed_nav #logo img").removeClass('animated fadeInDown');
jQuery(".cl_fixed_nav #main-nav").removeClass('animated fadeInUp');
$(window).scroll(function() {
  var scroll = $(window).scrollTop();
  var reduced_h1 = $(window).height() - 10;
    if( $(window).scrollTop() > reduced_h1 ) {
        //$('#header-wrap').css('opacity',1);
        jQuery(".home .cl_fixed_nav").removeClass('cl_H_fixed_nav');
        jQuery(".page-id-86 .cl_fixed_nav").removeClass('cl_H_fixed_nav');
        jQuery(".page-id-16 .cl_fixed_nav").removeClass('cl_H_fixed_nav');
    } else {
        //$('#header-wrap').css('opacity',0);   
         jQuery('.home .cl_fixed_nav').addClass('cl_H_fixed_nav');
         jQuery('.page-id-86 .cl_fixed_nav').addClass('cl_H_fixed_nav');
         jQuery('.page-id-16 .cl_fixed_nav').addClass('cl_H_fixed_nav');
    }
});

$(window).scroll(function() {
  var scroll = $(window).scrollTop();
  var reduced_h1_about = $(".banner_shdw").height();
    if( $(window).scrollTop() > reduced_h1_about ) {
        //$('#header-wrap').css('opacity',1);
        jQuery(".page-id-80 .cl_fixed_nav").removeClass('cl_H_fixed_nav');
        jQuery(".page-id-42 .cl_fixed_nav").removeClass('cl_H_fixed_nav');
    } else {
        //$('#header-wrap').css('opacity',0);   
         jQuery('.page-id-80 .cl_fixed_nav').addClass('cl_H_fixed_nav');
         jQuery('.page-id-42 .cl_fixed_nav').addClass('cl_H_fixed_nav');
    }
});


   jQuery('.fancy_scroll a[href^="/#"]').click(function() {
        jQuery('html,body').animate({
            scrollTop: jQuery(this.hash).offset().top
        }, 1200);
		return false;  // enable to hide the #tag in url bar
    });
	
	/*jQuery('.fancy_scroll a[href^="#"]').on('click', function (e) {
         e.preventDefault();
        var target = this.hash,
            $target = jQuery(target);

        jQuery('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 900, 'swing', function () {});
	});*/
	
	  var sections = $('section')
		, nav = $('nav')
		, nav_height = nav.outerHeight();
	   
	  $(window).scroll(function(e) {
		  //e.stopPropagation();
		  e.preventDefault();
		var cur_pos = $(this).scrollTop();
	   
		sections.each(function() {
		  var top = $(this).offset().top - nav_height,
			  bottom = top + $(this).outerHeight();
	   
		  if (cur_pos >= top && cur_pos <= bottom) {
			//nav.find('li').removeClass('current_page_item');
			$('.home .nav li').removeClass('current_page_item');
			//nav.find('li a[href="#'+$(this).attr('id')+'"]').parent('li').addClass('selected_menu');
			nav.find('a[href="/#'+$(this).attr('id')+'"]').parent('li').addClass('selected_menu'); 
		  }
		  else
		  {
			  nav.find('a[href="/#'+$(this).attr('id')+'"]').parent('li').removeClass('selected_menu'); 
		  }
		});
	  });
		


        jQuery('.cl_fixed_nav h3.nav-toggle').click(function () {
		  jQuery('.cl_fixed_nav #main-nav').slideToggle(500);
		  jQuery('.cl_fixed_nav .cl_logo').toggleClass('marginBtm15');
		  });
		
  // /// Top nav button 

/* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    Homepage banner Zoom Effect Class   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */  
  setTimeout(function () {
    $('.h_banner_bg').addClass('h_banner_bg_zoominout');
  }, 1000);
  
  //jQuery('.h_banner_bg').css('animation','50s linear 1s normal none infinite zoominout');
  
  
 /* (function($) {  
  
            var x = 0;  
            var y = 0;  
            //cache a reference to the banner  
            var banner = $(".h_banner_bg");  
  
            // set initial banner background position  
            banner.css('backgroundPosition', x + 'px' + ' ' + y + 'px');  
  
            // scroll up background position every 90 milliseconds  
            window.setInterval(function() {  
                banner.css("backgroundPosition", x + 'px' + ' ' + y + 'px');  
                y--;  
                //x--;  
  
                //if you need to scroll image horizontally -  
                // uncomment x and comment y  
  
            }, 90);  
  
})(jQuery);  
  */
  
  
  
  
/* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   ///  Homepage banner Zoom Effect Class  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
  
  /* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    Homepage product Slider   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
  
                 /* $('.j_h_p_s_Next').click(function() {
                    var j_p_id = $('.current_p').attr('id');
					console.log(j_p_id);
                    if (j_p_id == 'j_h_products_slider1')
                    {
						jQuery('.j_h_products_slider').hide();
						jQuery('.j_h_products_slider').removeClass('current_p');
						jQuery('#j_h_products_slider2').fadeIn();
						jQuery('#j_h_products_slider2').addClass('current_p');
                    }
                    else if (j_p_id == 'j_h_products_slider2')
                    {
						jQuery('.j_h_products_slider').hide();
						jQuery('.j_h_products_slider').removeClass('current_p');
						jQuery('#j_h_products_slider3').fadeIn();
						jQuery('#j_h_products_slider3').addClass('current_p');
                    }
					else
					{
						jQuery('.j_h_products_slider').hide();
						jQuery('.j_h_products_slider').removeClass('current_p');
						jQuery('#j_h_products_slider1').fadeIn();
						jQuery('#j_h_products_slider1').addClass('current_p');
					}
                });
				
				 $('.j_h_p_s_Prev').click(function() {  
                    var j_p_id = $('.current_p').attr('id');
					console.log(j_p_id);
                    if (j_p_id == 'j_h_products_slider3')
                    {
						jQuery('.j_h_products_slider').hide();
						jQuery('.j_h_products_slider').removeClass('current_p');
						jQuery('#j_h_products_slider2').fadeIn();
						jQuery('#j_h_products_slider2').addClass('current_p');
                    }
                    else if (j_p_id == 'j_h_products_slider2')
                    {
						jQuery('.j_h_products_slider').hide();
						jQuery('.j_h_products_slider').removeClass('current_p');
						jQuery('#j_h_products_slider1').fadeIn();
						jQuery('#j_h_products_slider1').addClass('current_p');
                    }
					else
					{
						jQuery('.j_h_products_slider').hide();
						jQuery('.j_h_products_slider').removeClass('current_p');
						jQuery('#j_h_products_slider3').fadeIn();
						jQuery('#j_h_products_slider3').addClass('current_p');
					}
                });*/
				
				
				 $('.j_h_p_s_Next').click(function() {
                    var j_p_id = $('.current_p').attr('id');
					//fIn_time = 300;
					//fOut_time = 300;
					console.log(j_p_id);
                    if (j_p_id == 'j_h_products_slider1')
                    {
				
				        jQuery('.j_h_products_slider').hide();
						jQuery('#j_h_products_slider2').fadeIn();
						
						jQuery('.j_h_products_slider').removeClass('current_p');
						jQuery('#j_h_products_slider2').addClass('current_p');
						//jQuery('.j_h_p_s_Prev').removeClass('j_h_p_s_NP_inactive');
                    }
                    else if (j_p_id == 'j_h_products_slider2')
                    {
					 jQuery('.j_h_products_slider').hide();
					 jQuery('#j_h_products_slider3').fadeIn();
					
						jQuery('.j_h_products_slider').removeClass('current_p');
						jQuery('#j_h_products_slider3').addClass('current_p');
						//jQuery('.j_h_p_s_Prev').removeClass('j_h_p_s_NP_inactive');
						//jQuery(this).addClass('j_h_p_s_NP_inactive')
                    }
					else
					{
					 jQuery('.j_h_products_slider').hide();
					 jQuery('#j_h_products_slider1').fadeIn();
					
						jQuery('.j_h_products_slider').removeClass('current_p');
						jQuery('#j_h_products_slider1').addClass('current_p');
						
					}
                });
				
				
				 $('.j_h_p_s_Prev').click(function() {  
                    var j_p_id = $('.current_p').attr('id');
					console.log(j_p_id);
                    if (j_p_id == 'j_h_products_slider3')
                    {
					 jQuery('.j_h_products_slider').hide();
					 jQuery('#j_h_products_slider2').fadeIn();
						
						jQuery('.j_h_products_slider').removeClass('current_p');
						jQuery('#j_h_products_slider2').addClass('current_p');
						//jQuery('.j_h_p_s_Next').removeClass('j_h_p_s_NP_inactive');
                    }
                    else if (j_p_id == 'j_h_products_slider2')
                    {
					 jQuery('.j_h_products_slider').hide();
					 jQuery('#j_h_products_slider1').fadeIn();
						
						jQuery('.j_h_products_slider').removeClass('current_p');
						jQuery('#j_h_products_slider1').addClass('current_p');
						//jQuery('.j_h_p_s_Next').removeClass('j_h_p_s_NP_inactive');
						//jQuery(this).addClass('j_h_p_s_NP_inactive')
                    }
					else
					{
					 jQuery('.j_h_products_slider').hide();
					 jQuery('#j_h_products_slider3').fadeIn();
						
						jQuery('.j_h_products_slider').removeClass('current_p');
						jQuery('#j_h_products_slider3').addClass('current_p');
					}
                });
				
/* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  ///  Homepage product Slider   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */	

/* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  Homepage Client Sec   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */

        var j_clients_testi_msg_height = jQuery('.j_clients_testi_msg li').height();
		jQuery('.j_clients_testi_msg').css('height',j_clients_testi_msg_height);
		jQuery(window).resize(function() {  
		 var j_clients_testi_msg_height = jQuery('.j_clients_testi_msg li').height();
		 jQuery('.j_clients_testi_msg').css('height',j_clients_testi_msg_height);
		});
		
//jQuery('.j_clients_testi_msg li').css('height','50');
   $(".j_clients_testi_msg li").each(function(i) {
     $(this).attr("id","j_clients_testi_msg"+(i+1));
    });
	
	$(".j_clients_testi_img li").each(function(i) {
     $(this).attr("rel","j_clients_testi_msg"+(i+1));
    });
	
	    jQuery(".j_clients_testi_img li").hover(function() {
		  jQuery(".j_clients_testi_msg li").removeClass('j_clients_testi_msg_active');
		  jQuery(".j_clients_testi_img li").removeClass('j_clients_testi_tab_active');
		  jQuery(".j_clients_testi_msg li").hide();
		  var selected_tab = jQuery(this).attr("rel");
		  jQuery("#"+selected_tab).addClass("j_clients_testi_msg_active");
		  jQuery(this).addClass("j_clients_testi_tab_active");
		  jQuery("#"+selected_tab).show();
		  return false;
		});
			

/* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  ///  Homepage Client Sec   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */	

/* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  Homepage Work Sec   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
 
    $(".cl_h_sec5 .work-list li").each(function(i) {
     $(this).attr("rel","cl_h_sec5_portf"+(i+1));
    });
	
	$(".cl_h_sec5_slider .slide_deiv").each(function(i) {
     $(this).attr("id","cl_h_sec5_portf"+(i+1));
    });
	
	
      jQuery('.cl_h_sec5 .work-list li').click(function() {
      
      jQuery(".cl_h_sec5 .work-list li").removeClass('cl_h_sec5_portf_tab_active');
		  jQuery(".cl_h_sec5_slider .slide_deiv").removeClass('cl_h_sec5_portf_desc_active');
		  jQuery(".cl_h_sec5_slider .slide_deiv .showc_cnt h3").removeClass('fade_in_right');
		  jQuery(".cl_h_sec5_slider .slide_deiv .showc_cnt p").removeClass('fade_in_right_delay');
		  jQuery(".cl_h_sec5_slider .slide_deiv .showc_cnt .click_app_links").removeClass('fade_in_right_delay');
		  var selected_tab = jQuery(this).attr("rel");
		  jQuery("#"+selected_tab).addClass("cl_h_sec5_portf_desc_active");
		  jQuery(this).addClass("cl_h_sec5_portf_tab_active");
		  jQuery(".cl_h_sec5_slider").diyslider("move", jQuery(".cl_h_sec5_portf_desc_active").attr("rel"));   
        
        jQuery('#cl_h_sec5_popup').bPopup({    
          //easing: 'easeOutBack', //uses jQuery easing plugin
          speed: 400,
          transition: 'fadeIn',
          zIndex: 2,
		  positionStyle: 'fixed',
          modalClose: true  //Should the popup close on click on overlay
            });
			//jQuery('.slide_deiv_sel').addClass('active');
			//jQuery(".cl_h_sec5_slider").diyslider("move", jQuery(this).children(".current-slide").val());
			//jQuery(".cl_h_sec5_slider").diyslider("move", 3);
	//jQuery(".cl_h_sec5_slider").diyslider("move", jQuery(".cl_h_sec5_portf_desc_active").attr("rel"));
			//alert(jQuery(".current-slide").attr("rel"));
			
		  
		  jQuery("#"+selected_tab+" .showc_cnt h3").addClass("fade_in_right");
		  jQuery("#"+selected_tab+" .showc_cnt p").addClass("fade_in_right_delay");
		  jQuery("#"+selected_tab+" .showc_cnt .click_app_links").addClass("fade_in_right_delay");
		  return false;
		  
         });
      jQuery('#cl_h_sec5_popup_close').click(function() {
        var bPopup = jQuery('#cl_h_sec5_popup').bPopup({
            easing: 'slideBack', //uses jQuery easing plugin
            speed: 650,
            transition: 'fadeIn'
            });
        bPopup.close(); 
          });
	

/* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ /// Homepage Work Sec   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */	
   
/*@@@@@@@@@@@@@@@@@@@@@@@@@@ platform section @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
      

      $(".cl_h_sec5 .platform-list li").each(function(i) {
     $(this).attr("rel","cl_h_sec5_platform_cat"+(i+1));
    });

      $(".cl_h_sec5_platform_slider .slide_platform_deiv").each(function(i) {
     $(this).attr("id","cl_h_sec5_platform_cat"+(i+1));
    });

      jQuery('.cl_h_sec5 .platform-list li').click(function() {
      
      jQuery(".cl_h_sec5 .platform-list li").removeClass('cl_h_sec5_platform_tab_active');
		  jQuery(".cl_h_sec5_platform_slider .slide_platform_deiv").removeClass('cl_h_sec5_platform_desc_active');
		  var selected_tab = jQuery(this).attr("rel");
		  console.log(selected_tab);
		  jQuery("#"+selected_tab).addClass("cl_h_sec5_platform_desc_active");
		  jQuery(this).addClass("cl_h_sec5_platform_tab_active");
		  jQuery(".cl_h_sec5_platform_slider").diyslider("move", jQuery(".cl_h_sec5_platform_desc_active").attr("rel"));   
        
        jQuery('#cl_h_sec5_platform_popup').bPopup({    
          //easing: 'easeOutBack', //uses jQuery easing plugin
          speed: 400,
          transition: 'fadeIn',
          zIndex: 2,
		  positionStyle: 'fixed',
          modalClose: true  //Should the popup close on click on overlay
            });
			//jQuery('.slide_deiv_sel').addClass('active');
			//jQuery(".cl_h_sec5_slider").diyslider("move", jQuery(this).children(".current-slide").val());
			//jQuery(".cl_h_sec5_slider").diyslider("move", 3);
	//jQuery(".cl_h_sec5_slider").diyslider("move", jQuery(".cl_h_sec5_portf_desc_active").attr("rel"));
			//alert(jQuery(".current-slide").attr("rel"));
		  return false;
		  
         });

jQuery('#cl_h_sec5_platform_popup_close').click(function() {
        var bPopup = jQuery('#cl_h_sec5_platform_popup').bPopup({
            easing: 'slideBack', //uses jQuery easing plugin
            speed: 650,
            transition: 'fadeIn'
            });
        bPopup.close(); 
          });
//ondemand
$(".platform_ondemand_testi_msg li").each(function(i) {
     $(this).attr("id","on_demand_testi_msg"+(i+1));
    });
	
	$(".platform_examples .platform_ondemand_work").each(function(i) {
     $(this).attr("rel","on_demand_testi_msg"+(i+1));
    });

      jQuery(".platform_examples .platform_ondemand_work").hover(function() {
		  jQuery(".platform_examples .platform_ondemand_work").removeClass('active_platform');
		  jQuery(".platform_ondemand_testi_msg li").removeClass('platform_ondemand_testi_msg_active');
		  jQuery(".platform_ondemand_testi_msg li").hide();
		  var selected_tab = jQuery(this).attr("rel");
		  jQuery("#"+selected_tab).addClass("platform_ondemand_testi_msg_active");
		  jQuery(this).addClass("active_platform");
		  jQuery("#"+selected_tab).show();
		  return false;
		});
//ondemand ends

//marketplaces

$(".platform_marketplace_testi_msg li").each(function(i) {
     $(this).attr("id","marketplace_testi_msg"+(i+1));
    });
	
	$(".platform_examples .platform_marketplace_work").each(function(i) {
     $(this).attr("rel","marketplace_testi_msg"+(i+1));
    });

      jQuery(".platform_examples .platform_marketplace_work").hover(function() {
		  jQuery(".platform_examples .platform_marketplace_work").removeClass('active_marketplace_platform');
		  jQuery(".platform_marketplace_testi_msg li").removeClass('platform_marketplace_testi_msg_active');
		  jQuery(".platform_marketplace_testi_msg li").hide();
		  var selected_tab = jQuery(this).attr("rel");
		  jQuery("#"+selected_tab).addClass("platform_marketplace_testi_msg_active");
		  jQuery(this).addClass("active_marketplace_platform");
		  jQuery("#"+selected_tab).show();
		  return false;
		});

//marketplaces end

//content

$(".platform_content_testi_msg li").each(function(i) {
     $(this).attr("id","content_testi_msg"+(i+1));
    });
	
	$(".platform_examples .platform_content_work").each(function(i) {
     $(this).attr("rel","content_testi_msg"+(i+1));
    });

      jQuery(".platform_examples .platform_content_work").hover(function() {
		  jQuery(".platform_examples .platform_content_work").removeClass('active_content_platform');
		  jQuery(".platform_content_testi_msg li").removeClass('platform_content_testi_msg_active');
		  jQuery(".platform_content_testi_msg li").hide();
		  var selected_tab = jQuery(this).attr("rel");
		  jQuery("#"+selected_tab).addClass("platform_content_testi_msg_active");
		  jQuery(this).addClass("active_content_platform");
		  jQuery("#"+selected_tab).show();
		  return false;
		});

//content end

//social

$(".platform_social_testi_msg li").each(function(i) {
     $(this).attr("id","social_testi_msg"+(i+1));
    });
	
	$(".platform_examples .platform_social_work").each(function(i) {
     $(this).attr("rel","social_testi_msg"+(i+1));
    });

      jQuery(".platform_examples .platform_social_work").hover(function() {
		  jQuery(".platform_examples .platform_social_work").removeClass('active_social_platform');
		  jQuery(".platform_social_testi_msg li").removeClass('platform_social_testi_msg_active');
		  jQuery(".platform_social_testi_msg li").hide();
		  var selected_tab = jQuery(this).attr("rel");
		  jQuery("#"+selected_tab).addClass("platform_social_testi_msg_active");
		  jQuery(this).addClass("active_social_platform");
		  jQuery("#"+selected_tab).show();
		  return false;
		});

//social end

//communnication

$(".platform_communication_testi_msg li").each(function(i) {
     $(this).attr("id","commun_testi_msg"+(i+1));
    });
	
	$(".platform_examples .platform_communication_work").each(function(i) {
     $(this).attr("rel","commun_testi_msg"+(i+1));
    });

      jQuery(".platform_examples .platform_communication_work").hover(function() {
		  jQuery(".platform_examples .platform_communication_work").removeClass('active_communication_platform');
		  jQuery(".platform_communication_testi_msg li").removeClass('platform_communication_testi_msg_active');
		  jQuery(".platform_communication_testi_msg li").hide();
		  var selected_tab = jQuery(this).attr("rel");
		  jQuery("#"+selected_tab).addClass("platform_communication_testi_msg_active");
		  jQuery(this).addClass("active_communication_platform");
		  jQuery("#"+selected_tab).show();
		  return false;
		});

//communnication ends

//iot

$(".platform_iot_testi_msg li").each(function(i) {
     $(this).attr("id","iot_testi_msg"+(i+1));
    });
	
	$(".platform_examples .platform_iot_work").each(function(i) {
     $(this).attr("rel","iot_testi_msg"+(i+1));
    });

      jQuery(".platform_examples .platform_iot_work").hover(function() {
		  jQuery(".platform_examples .platform_iot_work").removeClass('active_iot_platform');
		  jQuery(".platform_iot_testi_msg li").removeClass('platform_iot_testi_msg_active');
		  jQuery(".platform_iot_testi_msg li").hide();
		  var selected_tab = jQuery(this).attr("rel");
		  jQuery("#"+selected_tab).addClass("platform_iot_testi_msg_active");
		  jQuery(this).addClass("active_iot_platform");
		  jQuery("#"+selected_tab).show();
		  return false;
		});

//iot ends

/*@@@@@@@@@@@@@@@@@@@@@@@@@@ platform section ends @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/   
   

});  // ready fn() end...

/* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ /// Career Page Jquery   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */

// JavaScript Document

//Use onclick="slideup('#idname');"
function slideup(id) {
    jQuery( 'html, body' ).animate({ scrollTop: jQuery( '#' + id ).offset().top - 65}, 800 );
	 
  }
  
  
// //	The menu on the left
// jQuery(function() {
//   jQuery('nav#menu-left').mmenu();
// });

screen_W = jQuery(window).width();
screen_H = jQuery(window).height();

jQuery(window).load(function(){
  var mar_top = screen_H / 2;
  var reduced_h = screen_H - 100;
  
  //jQuery('').css({'height':reduced_h,'width':'100%'});

});


jQuery(document).ready(function() {
	
	// Set height dynamic according to screen height
	jQuery(window).resize(function() {
		jQuery('.full_bg_img').height(jQuery(window).height() - 60);
		jQuery('.banner_content').height(jQuery(window).height() - 70);
	});
	jQuery(window).trigger('resize');	
	
	
	/* // Slide up with id in url
	jQuery('a[href^="#"]').click(function() {
        jQuery('html,body').animate({
            scrollTop: jQuery(this.hash).offset().top
        }, 1200);
		return false;
    });*/
	
	// Slide up without id in url
	jQuery('.slideup_content a[href^="#"]').on('click', function (e) {
         e.preventDefault();
        var target = this.hash,
            $target = jQuery(target);

        jQuery('html, body').stop().animate({
            'scrollTop': $target.offset().top - 65
        }, 900, 'swing', function () {});
		
		var c_cat_tab = jQuery(this).attr("rel");
		//alert(c_cat_tab);
		jQuery("#" + c_cat_tab).click();
		
		//jQuery( $target ).trigger( "click" );
		
        return false;
    });
	
	

// Career Post drop down
           jQuery(".career_cat ul li").click(function() {
				jQuery(".career_cat ul li").removeClass('career_cat_active');
				jQuery(this).addClass("career_cat_active");
				jQuery(".career_dropdown ol").hide();
				var selected_tab = jQuery(this).find("a").attr("href");
				jQuery(selected_tab).fadeIn(800);
				return false;
			});	
			
			
			
			/*jQuery('.banner_text_buttom li').click(function() {
				var c_cat_tab = jQuery(this).find("a").attr("rel");
				alert(c_cat_tab);
				jQuery("#" + c_cat_tab).click();
				//jQuery(c_cat_tab).trigger( "click" );
			});*/
			
			
			
		   jQuery(".career_dropdown .career_post_title").click(function () {			   		   
			   jQuery(".career_dropdown .career_post").css('display','none');
//			   jQuery(".career_dropdown .career_post_title").removeClass( "expanded" );  
//			   jQuery(this).next(".career_dropdown .career_post").slideDown(600);
					
				if(jQuery(this).hasClass('expanded'))
				{
					jQuery(this).removeClass( "expanded" ); 
			    	jQuery(this).next(".career_dropdown .career_post").slideUp(600);					
				}
				else{	
				     jQuery(".career_dropdown .career_post_title").removeClass( "expanded" ); 
					 jQuery(this).addClass( "expanded" );
					 jQuery(this).next(".career_dropdown .career_post").slideDown(600);	  
				}

 	          jQuery( 'html, body' ).animate({ scrollTop: jQuery(this).offset().top - 100}, 800 );
			  //jQuery(this).next(".career_post").slideToggle(500);
		   });
		   
		   
		   
			// set the input value of apply for field on button click
			jQuery(".career_dropdown button").click(function () {   
			var apply_for_btn = jQuery(this).attr("rel");
			jQuery('#InterestedIn').val(apply_for_btn);
			});

});


/* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ /// Career Page Jquery   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
/*
================================================================================
***********       Career Form validation            ***********
================================================================================
*/

//
//First Form Validation for Mobile gaming.
// jQuery(document).ready(function() {
//       jQuery("#CareerSubmit").click(function() {
//           // Variable declaration
//            var gError = false;
// 			var letters = /^[A-Za-z_ ]+$/;
//            var bcName = document.bcForm.bcName.value;
// 			var bcEmail = document.bcForm.bcEmail.value;
// 			var atpos=bcEmail.indexOf("@");
//            var dotpos=bcEmail.lastIndexOf(".");
// 			var bcQualification = document.bcForm.bcQualification.value;
// 			//var bcCollege = document.bcForm.bcCollege.value;
// 			var numeric = /^[0-9+-]+$/;
// 			var bcPhone = document.bcForm.bcPhone.value;
// 			var cv = document.getElementById('fileInput').value;
// 			var fileInput = document.getElementById('fileInput');
//          	// Form field validation
//             if(bcName.length == 0){
// 				var gError = true;
// 				document.getElementById("bcName").style.backgroundColor="#F8CCCC";
//                 return false;
// 			     } 
				 
// 					  if(bcName.match(letters)) 
// 					  { 
// 					  document.getElementById("bcName").style.backgroundColor="white";
// 					  } 
// 					  else 
// 					  { 
// 					  var gError = true;
// 					  document.getElementById("bcName").style.backgroundColor="#F8CCCC";
// 					  return false; 
// 					  } 
					  
			
// 			if(bcEmail==0)
// 			{
// 			document.getElementById("bcEmail").style.backgroundColor="#F8CCCC";
// 			return false;
// 			}
			
// 				  if (atpos<1 || dotpos<atpos+2 || dotpos+2>=bcEmail.length)
// 					{
// 					var gError = true;
// 					document.getElementById("bcEmail").style.backgroundColor="#F8CCCC";
// 					return false;
// 					}
// 					if(bcEmail!=0)
// 					{
// 					  document.getElementById("bcEmail").style.backgroundColor="white";
// 					}
					
					
// 			 if(bcPhone.match(numeric) || bcPhone==0) 
// 			         {	}
// 					  else 
// 					  {
// 					  var gError = true;
// 					  document.getElementById("bcPhone").style.backgroundColor="#F8CCCC";
// 					  return false; 
// 					  } 		
			
// 			if(bcQualification==0)
// 			{
// 			var gError = true;
// 			document.getElementById("bcQualification").style.backgroundColor="#F8CCCC";
// 			return false;
// 			}
// 			else
// 			{
// 				document.getElementById("bcQualification").style.backgroundColor="white";
// 			}
			
			
// 			if( cv == '' )
// 			{
// 			var gError = true;
// 			document.getElementById("max_size").innerHTML="Upload your CV";
// 			return false;
// 			}
	
			
// 			 for (var i = 0; i < fileInput .files.length; i++) 
// 		   {  
//              var file = fileInput.files[i];
// 				if ('size' in file) 
// 					{
// 	                   var sz=file.size/(1024*1024);
// 	                   //RESTRICTING UPLOAD FILE SIZE TO 2MB
//                     	   if(sz >= 2)
// 								{
// 									//alert('Document is not UPLOADED. Maximum file size is restricted to 2MB.Please try again Uploading other document!!');
// 									 document.getElementById("max_size").innerHTML="Upload max-size 2mb";
//                                      return false;
// 								 }
// 					}
// 		   }//for

// 				//Career_Mail();		
			
// 			 if(gError == false){
// 				//alert("Sucecss");
//     jQuery(".submit").click(function(){
// 	this.value='Submitting ..';
// 	this.disabled='disabled';
//     });

// 			 }
       
//   });
// });

//First Form Validation for Mobile gaming.
       function onSubmitCareer(){
          // Variable declaration
           var gError = false;
			var letters = /^[A-Za-z_ ]+$/;
           var bcName = document.bcForm.bcName.value;
			var bcEmail = document.bcForm.bcEmail.value;
			var atpos=bcEmail.indexOf("@");
           var dotpos=bcEmail.lastIndexOf(".");
			var bcQualification = document.bcForm.bcQualification.value;
			//var bcCollege = document.bcForm.bcCollege.value;
			var numeric = /^[0-9+-]+$/;
			var bcPhone = document.bcForm.bcPhone.value;
			var cv = document.getElementById('fileInput').value;
			var fileInput = document.getElementById('fileInput');
         	// Form field validation
            if(bcName.length == 0){
				var gError = true;
				document.getElementById("bcName").style.backgroundColor="#F8CCCC";
                return false;
			     } 
				 
					  if(bcName.match(letters)) 
					  { 
					  document.getElementById("bcName").style.backgroundColor="white";
					  } 
					  else 
					  { 
					  var gError = true;
					  document.getElementById("bcName").style.backgroundColor="#F8CCCC";
					  return false; 
					  } 
					  
			
			if(bcEmail==0)
			{
			document.getElementById("bcEmail").style.backgroundColor="#F8CCCC";
			return false;
			}
			
				  if (atpos<1 || dotpos<atpos+2 || dotpos+2>=bcEmail.length)
					{
					var gError = true;
					document.getElementById("bcEmail").style.backgroundColor="#F8CCCC";
					return false;
					}
					if(bcEmail!=0)
					{
					  document.getElementById("bcEmail").style.backgroundColor="white";
					}
					
					
			 if(bcPhone.match(numeric) || bcPhone==0) 
			         {	}
					  else 
					  {
					  var gError = true;
					  document.getElementById("bcPhone").style.backgroundColor="#F8CCCC";
					  return false; 
					  } 		
			
			if(bcQualification==0)
			{
			var gError = true;
			document.getElementById("bcQualification").style.backgroundColor="#F8CCCC";
			return false;
			}
			else
			{
				document.getElementById("bcQualification").style.backgroundColor="white";
			}
			
			
			if( cv == '' )
			{
			var gError = true;
			document.getElementById("max_size").innerHTML="Upload your CV";
			return false;
			}
	
			
			 for (var i = 0; i < fileInput .files.length; i++) 
		   {  
             var file = fileInput.files[i];
				if ('size' in file) 
					{
	                   var sz=file.size/(1024*1024);
	                   //RESTRICTING UPLOAD FILE SIZE TO 2MB
                    	   if(sz >= 2)
								{
									//alert('Document is not UPLOADED. Maximum file size is restricted to 2MB.Please try again Uploading other document!!');
									 document.getElementById("max_size").innerHTML="Upload max-size 2mb";
                                     return false;
								 }
					}
		   }//for

						
			
			 if(gError == false){
				//alert("Sucecss");
    jQuery(".submit").click(function(){
	this.value='Submitting ..';
	this.disabled='disabled';
    });

			 }
       
  }
  
// Change color of the input field on onClick. 
  function changecolor(id)
  {
	document.getElementById(id).style.backgroundColor='white';
  }
  
   function colorchange(id)
  {
	document.getElementById(id).style.backgroundColor='transparent';
  }
